﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoNews.Models
{
    public class CryptocurrencyModel
    {
        public string Name { get; set; }

        public string FullName { get; set; }

        public string PictureUrl { get; set; }

        public string Price { get; set; }

        public bool Subscribed { get; set; }
    }
}
