﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CryptoNews.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;


namespace CryptoNews.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {
            var connectionString = "mongodb+srv://CryptoNews:HMQRqKkYQAoll2kF@cluster0-1dfrx.mongodb.net/test?retryWrites=true";
            var client = new MongoClient(connectionString);

             this.db = client.GetDatabase("CryptoNews");
        }

        private IMongoDatabase db;

        public async Task<IActionResult> Index()
        {

            List<CryptocurrencyModel> cryptocurrencies = new List<CryptocurrencyModel>();
            List<string> userCryptocurrencies = new List<string>();
            var name = HttpContext.Session.GetString("User");
            @ViewBag.user = HttpContext.Session.GetString("User");
            var endpoint = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=";


            if (string.IsNullOrEmpty(name))
            {
                @ViewBag.Message = "Prisijunkite jog matytumėte savo valiutas";
                return View(cryptocurrencies);
            }

            var collection = db.GetCollection<BsonDocument>("UserCryptocurrencies");

            var filter = new BsonDocument("user", name);

            var document = new BsonDocument();


            await collection.Find(filter)
                .ForEachAsync(x => cryptocurrencies.Add(new CryptocurrencyModel()
                {
                    FullName = x["name"].ToString(),
                    Name = x["code"].ToString(),
                    PictureUrl = x["picture"].ToString()
                }));

            if (cryptocurrencies.Any())
            {
                foreach (var a in document)
                {
                    
                }
                var cryptocurrencyAsync = GetCryptocurrenciesPrices(cryptocurrencies);
                    cryptocurrencyAsync.Wait();
                    var cryptocurrenciesWithPrices = cryptocurrencyAsync.Result;
                    //userCryptocurrencies.Add(document["code"].ToString());
                
                return View(cryptocurrenciesWithPrices);
            }
            else
            {
                @ViewBag.Message = "Jus neturite pasirinke jokių kriptovaliutų";
            }

            return View(cryptocurrencies);
        }



        public async Task<List<CryptocurrencyModel>> GetCryptocurrenciesPrices(List<CryptocurrencyModel> cryptocurrencies)
        {

            var endpoint = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=";
            foreach (var crypto in cryptocurrencies)
            {
                endpoint += crypto.Name + ",";
            }
            endpoint = endpoint.Remove(endpoint.Length - 1);
            endpoint += "&tsyms=EUR&api_key=5af932940cdd67c449d07634b1ced0336e7c2a5385f5da0e6a1a6ea9b8e95a5c";

            using (var httpClient = new HttpClient())
            {



                //var byteArray = Encoding.ASCII.GetBytes(configuration["1Membership:Name"] + ":" + configuration["1Membership:ApiKey"]);

                //httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));

                string response = await httpClient.GetStringAsync(new Uri(endpoint));

                dynamic releases = JsonConvert.DeserializeObject(response);

                foreach (var crypto in cryptocurrencies)
                {
                    crypto.Price = releases[crypto.Name].EUR;
                }

                return cryptocurrencies;
            }

        }



        public async Task<List<CryptocurrencyModel>> GetCryptocurrencies()
        {

            using (var httpClient = new HttpClient())
            {

                if (HttpContext.Session.GetString("User") != null)
                {
                    @ViewBag.user = HttpContext.Session.GetString("User");
                }

                //var byteArray = Encoding.ASCII.GetBytes(configuration["1Membership:Name"] + ":" + configuration["1Membership:ApiKey"]);

                //httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(byteArray));

                string response = await httpClient.GetStringAsync(new Uri($"https://min-api.cryptocompare.com/data/top/totalvolfull?limit=100&tsym=EUR&api_key=5af932940cdd67c449d07634b1ced0336e7c2a5385f5da0e6a1a6ea9b8e95a5c"));

                dynamic releases = JsonConvert.DeserializeObject(response);
                var cryptocurrencies = new List<CryptocurrencyModel>();

                foreach (var crypto in releases.Data)
                {
                    cryptocurrencies.Add(new CryptocurrencyModel()
                    {
                        Name = crypto.CoinInfo.Name,
                        FullName = crypto.CoinInfo.FullName,
                        Price = crypto.DISPLAY.EUR.PRICE,
                        PictureUrl = "https://www.cryptocompare.com" + crypto.CoinInfo.ImageUrl
                    });
                }


                return cryptocurrencies;
            }

        }




        public IActionResult Login( string Message)
        {
            @ViewBag.Message = Message;
            return View();
        }

        public async Task<IActionResult> GetLoginData(string name, string password)
        {
            var collection = db.GetCollection<BsonDocument>("Users");

            var filter = new BsonDocument("name", name);

            var document = new BsonDocument();
            

           await collection.Find(filter)
                .ForEachAsync(x => document = x);

            if (document.Any())
            {
                if (document["password"] == password)
                {
                    HttpContext.Session.SetString("User", name);

                    return RedirectToAction("Index");
                }
                return RedirectToAction("Login", new { Message = "Neteisingi Duomenys" });
            }

            return RedirectToAction("Login", new { Message = "Neteisingi Duomenys" });
        }




        public async Task<IActionResult> GetRegisterData(string name, string password)
        {
            var collection = db.GetCollection<BsonDocument>("Users");

            var filter = new BsonDocument("name", name);

            var document = new BsonDocument();


            await collection.Find(filter)
                .ForEachAsync(x => document = x);

            if (document.Any())
            {
                return RedirectToAction("Register", new {Message = "toks vartotojas jau ekzistuoja"});

            }
            else
            {
                document = new BsonDocument();
                document.Add("name", name);
                document.Add("password", password);

                await collection.InsertOneAsync(document);

                HttpContext.Session.SetString("User", name);
            }


            return RedirectToAction("Index");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("User");

            return RedirectToAction("Index");
        }
        public IActionResult Register(string Message)
        {
            @ViewBag.Message = Message;
            return View();
        }

        public async Task<IActionResult> Cryptocurrencies()
        {
            var cryptoList = new List<string>();

            if (HttpContext.Session.GetString("User") != null)
            {
                @ViewBag.user = HttpContext.Session.GetString("User");


                var usercollection = db.GetCollection<BsonDocument>("UserCryptocurrencies");

                var filter = new BsonDocument("user", HttpContext.Session.GetString("User"));

                var document = new BsonDocument();


                await usercollection.Find(filter)
                    .ForEachAsync(x => cryptoList.Add(x["code"].ToString()));

            }


            List<CryptocurrencyModel> cryptocurrencies = new List<CryptocurrencyModel>();


            var collection = db.GetCollection<BsonDocument>("Cryptocurrencies");


            using (IAsyncCursor<BsonDocument> cursor = await collection.FindAsync(new BsonDocument()))
            {
                while (await cursor.MoveNextAsync())
                {
                    IEnumerable<BsonDocument> batch = cursor.Current;
                    foreach (BsonDocument document in batch)
                    {
                        cryptocurrencies.Add(new CryptocurrencyModel()
                        {
                            FullName = document["name"].ToString(),
                            Name = document["code"].ToString(),
                            PictureUrl = document["picture"].ToString(),
                            Subscribed = cryptoList.Any(x => x.Contains(document["code"].ToString()))

                        });
                    }
                }
            }

            


            cryptocurrencies = cryptocurrencies.Take(50).ToList();
            var cryptocurrencyAsync = GetCryptocurrenciesPrices(cryptocurrencies);
                    cryptocurrencyAsync.Wait();
                    var cryptocurrenciesWithPrices = cryptocurrencyAsync.Result;
                    //userCryptocurrencies.Add(document["code"].ToString());

                    return View(cryptocurrencies);

          
            

        }

        public async Task<IActionResult> Subscribe(string code, string name, string url)
        {

            var collection = db.GetCollection<BsonDocument>("UserCryptocurrencies");

            var document = new BsonDocument();
            document.Add("code", code);
            document.Add("name", name);
            document.Add("picture", url);
            document.Add("user", HttpContext.Session.GetString("User"));

            await collection.InsertOneAsync(document);
           

            return RedirectToAction("Cryptocurrencies");
        }

        public IActionResult Compare()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
