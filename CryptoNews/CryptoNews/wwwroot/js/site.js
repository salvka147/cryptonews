﻿$(document).ready(function () {


    $("#create-button").click(function () {


        var password = $(".password-create").val();
        var name = $(".name-create").val();
        var confirmPassword = $(".confirmpassword-create").val();

        if (password.length > 0 &&
            confirmPassword.length > 0 &&
            name.length > 0 &&
            password.length > 6 &&
            password === confirmPassword
        ) {
            var location = $("#create-button-get").find("a").attr("href").replace("namechange", name)
                .replace("passwordchange", password);
            window.location.href = location;
        } else {
            $("#error-tag").removeClass("hidden");
        }
    });

    $("#login-button").click(function () {


        var password = $(".password-login").val();
        var name = $(".name-login").val();

        if (password.length > 0 &&
            name.length > 0 
        ) {
            var location = $("#login-button-get").find("a").attr("href").replace("namechange", name)
                .replace("passwordchange", password);
            window.location.href = location;
        } else {
            $("#error-tag").removeClass("hidden");
        }
    });


    $(".subscribe-button").click(function () {


        var code = $(this).parent().find(".hidden-code").html();
        var name = $(this).parent().find(".hidden-name").html();
        var url = $(this).parent().find(".hidden-url").html();

        var location = $("#subscribe-button").find("a").attr("href").replace("codechange", code)
            .replace("namechange", name).replace("urlchange", url);
        window.location.href = location;
    });

});